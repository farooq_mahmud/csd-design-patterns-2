﻿namespace WorkItemTracker
{
    public abstract class State : IState
    {
        protected State(WorkItem workItem)
        {
            WorkItem = workItem;
        }

        protected WorkItem WorkItem { get; }

        public abstract void MakeProposed();
        public abstract void MakeActive();
        public abstract void MakeResolved();
        public abstract void MakeClosed();
    }
}