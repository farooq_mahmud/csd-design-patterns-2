﻿namespace CoffeeLib
{
    public class WithChocolate : CoffeeDecorator
    {
        public WithChocolate(Coffee decoratedCoffee) : base(decoratedCoffee)
        {
        }

        public override string Description => $"{base.Description} with chocolate";
        public override decimal Cost => base.Cost + 0.05M;
    }
}