﻿using System;

namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public class PaymentProcessorService : IPaymentProcessorService
    {
        public PaymentProcessorResponse ProcessPayment(Order order)
        {
            switch (order.PaymentType)
            {
                case PaymentType.CreditCard:
                    return new PaymentProcessorResponse {ConfirmationCode = "cc_123456"};
                case PaymentType.Paypal:
                    return new PaymentProcessorResponse {ConfirmationCode = "pp_123456"};
                default:
                    throw new InvalidOperationException("Invalid payment type.");
            }
        }
    }
}