﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorkItemTracker.UnitTests
{
    [TestClass]
    public class WorkItemUnitTests
    {
        [TestMethod]
        public void VerifyStateTransitions()
        {
            var wi = new WorkItem();

            wi.MakeActive();
            Assert.IsInstanceOfType(wi.CurrentState, typeof(ActiveState));

            wi.Resolve();
            Assert.IsInstanceOfType(wi.CurrentState, typeof(ResolvedState));

            wi.Close();
            Assert.IsInstanceOfType(wi.CurrentState, typeof(ClosedState));

            wi.Delete();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Delete_WhenCurrentStateIsProposed_ThrowsException()
        {
            var wi = new WorkItem();
            wi.CurrentState = new ProposedState(wi);
            wi.Delete();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Delete_WhenCurrentStateIsActive_ThrowsException()
        {
            var wi = new WorkItem();
            wi.CurrentState = new ActiveState(wi);
            wi.Delete();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Delete_WhenCurrentStateIsResolved_ThrowsException()
        {
            var wi = new WorkItem();
            wi.CurrentState = new ResolvedState(wi);
            wi.Delete();
        }
    }
}
