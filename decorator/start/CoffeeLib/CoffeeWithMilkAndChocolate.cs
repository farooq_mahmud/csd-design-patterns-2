﻿namespace CoffeeLib
{
    public class CoffeeWithMilkAndChocolate : CoffeeWithMilk
    {
        public override string Description => $"{base.Description} with chocolate";
        public override decimal Cost => base.Cost + 0.05M;
    }
}