﻿namespace CoffeeLib
{
    public class WithMilk : CoffeeDecorator
    {
        public WithMilk(Coffee decoratedCoffee) : base(decoratedCoffee)
        {
        }

        public override string Description => $"{base.Description} with milk";
        public override decimal Cost => base.Cost + 0.25M;
    }
}