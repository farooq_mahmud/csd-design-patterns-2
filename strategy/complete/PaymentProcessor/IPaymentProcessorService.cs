﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public interface IPaymentProcessorService
    {
        PaymentProcessorResponse ProcessPayment(Order order);
    }
}