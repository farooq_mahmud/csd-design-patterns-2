﻿namespace CoffeeLib
{
    public abstract class Drink
    {
        public abstract string Description { get;}
        public abstract decimal Cost { get;}
    }
}