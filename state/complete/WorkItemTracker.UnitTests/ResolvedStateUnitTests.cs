﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorkItemTracker.UnitTests
{
    [TestClass]
    public class ResolvedStateUnitTests
    {
        private WorkItem _workItem;

        [TestInitialize]
        public void TestInitialize()
        {
            _workItem = new WorkItem();
            _workItem.CurrentState = new ResolvedState(_workItem);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MakeProposed_ThrowsException()
        {
            _workItem.CurrentState.MakeProposed();
        }

        [TestMethod]
        public void MakeActive_SetsWorkItemStateToActive()
        {
            _workItem.CurrentState.MakeActive();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ActiveState));
        }

        [TestMethod]
        public void MakeResolved_SetsWorkItemStateToResolved()
        {
            _workItem.CurrentState.MakeResolved();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ResolvedState));
        }

        [TestMethod]
        public void MakeClosed_SetsWorkItemStateToResolved()
        {
            _workItem.CurrentState.MakeClosed();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ClosedState));
        }
    }
}
