﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeLib.UnitTests
{
    [TestClass]
    public class CoffeeUnitTests
    {
        private readonly Coffee _coffee = new Coffee();

        [TestMethod]
        public void Can_Get_The_Description()
        {
            Assert.AreEqual("coffee black", _coffee.Description);
        }

        [TestMethod]
        public void Base_Cost_Of_Coffee_Is_150()
        {
            Assert.AreEqual(1.5M, _coffee.Cost);
        }
    }
}