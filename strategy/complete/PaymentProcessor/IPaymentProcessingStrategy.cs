﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public interface IPaymentProcessingStrategy
    {
        PaymentProcessorResponse ProcessPayment(Order order);
    }
}