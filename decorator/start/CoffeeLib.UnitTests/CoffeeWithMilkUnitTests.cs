﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeLib.UnitTests
{
    [TestClass]
    public class CoffeeWithMilkUnitTests
    {
        private readonly CoffeeWithMilk _coffeeWithMilk = new CoffeeWithMilk();

        [TestMethod]
        public void Can_Get_The_Description()
        {
            Assert.AreEqual("coffee black with milk", _coffeeWithMilk.Description);
        }

        [TestMethod]
        public void Cost_Is_175()
        {
            Assert.AreEqual(1.75M, _coffeeWithMilk.Cost);
        }
    }
}