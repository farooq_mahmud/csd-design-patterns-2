﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeLib.UnitTests
{
    [TestClass]
    public class CoffeeWithMilkAndWhipUnitTests
    {
        private readonly CoffeeWithMilkAndWhip _coffeeWithMilkAndWhip = new CoffeeWithMilkAndWhip();

        [TestMethod]
        public void Can_Get_The_Description()
        {
            Assert.AreEqual("coffee black with milk with whip", _coffeeWithMilkAndWhip.Description);
        }

        [TestMethod]
        public void Cost_Is_185()
        {
            Assert.AreEqual(1.85M, _coffeeWithMilkAndWhip.Cost);
        }
    }
}