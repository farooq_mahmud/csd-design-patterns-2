﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeLib.UnitTests
{
    [TestClass]
    public class CoffeeDecoratorUnitTests
    {
        private Coffee _coffee;

        [TestInitialize]
        public void TestInitialize()
        {
            _coffee = new Coffee();
        }

        [TestMethod]
        public void Verify_Black_Coffee()
        {
            Assert.AreEqual("coffee black", _coffee.Description);
            Assert.AreEqual(1.5M, _coffee.Cost);
        }

        [TestMethod]
        public void Verify_Coffee_With_Milk()
        {
            var withMilk = new WithMilk(_coffee);
            Assert.AreEqual("coffee black with milk", withMilk.Description);
            Assert.AreEqual(1.75M, withMilk.Cost);
        }

        [TestMethod]
        public void Verify_Coffee_With_Milk_And_Chocolate()
        {
            var withMilk = new WithMilk(_coffee);
            var withChocolate = new WithChocolate(withMilk);
            Assert.AreEqual("coffee black with milk with chocolate", withChocolate.Description);
            Assert.AreEqual(1.80M, withChocolate.Cost);
        }

        [TestMethod]
        public void Verify_Coffee_With_Milk_And_Chocolate_And_Whip()
        {
            var withMilk = new WithMilk(_coffee);
            var withChocolate = new WithChocolate(withMilk);
            var withWhip = new WithWhip(withChocolate);
            Assert.AreEqual("coffee black with milk with chocolate with whip", withWhip.Description);
            Assert.AreEqual(1.90M, withWhip.Cost);
        }
    }
}