﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorkItemTracker.UnitTests
{
    [TestClass]
    public class WorkItemUnitTests
    {
        [TestMethod]
        public void When_A_New_WorkItem_Is_Created_Its_State_Is_Proposed()
        {
            Assert.AreEqual(WorkItemState.Proposed, new WorkItem().CurrentState);
        }

        [TestMethod]
        public void Verify_MakeActive()
        {
            var wi = new WorkItem();
            wi.MakeActive();
            Assert.AreEqual(WorkItemState.Active, wi.CurrentState);

            wi.CurrentState = WorkItemState.Resolved;
            wi.MakeActive();
            Assert.AreEqual(WorkItemState.Active, wi.CurrentState);

            wi.CurrentState = WorkItemState.Active;
            VerifyException(() => wi.MakeActive(), typeof(InvalidOperationException));
            
            wi.CurrentState = WorkItemState.Closed;
            VerifyException(() => wi.MakeActive(), typeof(InvalidOperationException));
        }

        [TestMethod]
        public void Verify_Resolve()
        {
            var wi = new WorkItem();
            VerifyException(() => wi.Resolve(), typeof(InvalidOperationException));
            
            wi.CurrentState = WorkItemState.Resolved;
            VerifyException(() => wi.Resolve(), typeof(InvalidOperationException));

            wi.CurrentState = WorkItemState.Closed;
            VerifyException(() => wi.Resolve(), typeof(InvalidOperationException));

            wi.CurrentState = WorkItemState.Active;
            wi.Resolve();
            Assert.AreEqual(WorkItemState.Resolved, wi.CurrentState);
        }

        [TestMethod]
        public void Verify_Close()
        {
            var wi = new WorkItem();
            wi.Close();
            Assert.AreEqual(WorkItemState.Closed, wi.CurrentState);

            wi.CurrentState = WorkItemState.Active;
            VerifyException(() => wi.Close(), typeof(InvalidOperationException));

            wi.CurrentState = WorkItemState.Resolved;
            wi.Close();
            Assert.AreEqual(WorkItemState.Closed, wi.CurrentState);

            wi.CurrentState = WorkItemState.Closed;
            VerifyException(() => wi.Close(), typeof(InvalidOperationException));
        }

        [TestMethod]
        public void Verify_Delete()
        {
            var wi = new WorkItem();
            wi.Close();
            wi.Delete();

            wi.CurrentState = WorkItemState.Proposed;
            VerifyException(() => wi.Delete(), typeof(InvalidOperationException));

            wi.CurrentState = WorkItemState.Active;
            VerifyException(() => wi.Delete(), typeof(InvalidOperationException));

            wi.CurrentState = WorkItemState.Resolved;
            VerifyException(() => wi.Delete(), typeof(InvalidOperationException));
        }

        private static void VerifyException(Action action, Type expectedException)
        {
            try
            {
                action();
            }
            catch (Exception e)
            {
                Assert.AreEqual(expectedException, e.GetType());
            }
        }
    }
}