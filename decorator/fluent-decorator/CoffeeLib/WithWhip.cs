﻿namespace CoffeeLib
{
    public class WithWhip : CoffeeDecorator
    {
        public WithWhip(Coffee decoratedCoffee) : base(decoratedCoffee)
        {
        }

        public override string Description => $"{base.Description} with whip";
        public override decimal Cost => base.Cost + 0.10M;
    }
}