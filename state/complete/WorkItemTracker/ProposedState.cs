﻿using System;

namespace WorkItemTracker
{
    public class ProposedState : State
    {
        public ProposedState(WorkItem workItem) : base(workItem)
        {
        }

        public override void MakeProposed()
        {
            WorkItem.CurrentState = WorkItem.Proposed;
        }

        public override void MakeActive()
        {
            WorkItem.CurrentState = WorkItem.Active;
        }

        public override void MakeResolved()
        {
            throw new InvalidOperationException("Work items in proposed state cannot be resolved.");
        }

        public override void MakeClosed()
        {
            WorkItem.CurrentState = WorkItem.Closed;
        }
    }
}