﻿using System;

namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public class PaymentProcessorService : IPaymentProcessorService
    {
        private readonly Func<Order, PaymentProcessorResponse> _paymentProcessingStrategy;

        public PaymentProcessorService(Func<Order, PaymentProcessorResponse> paymentProcessingStrategy)
        {
            _paymentProcessingStrategy = paymentProcessingStrategy;
        }

        public PaymentProcessorResponse ProcessPayment(Order order)
        {
            return _paymentProcessingStrategy(order);
        }
    }
}