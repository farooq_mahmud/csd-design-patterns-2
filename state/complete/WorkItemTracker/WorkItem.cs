﻿using System;

namespace WorkItemTracker
{
    public class WorkItem
    {
        public IState Proposed { get; }
        public IState Active { get; }
        public IState Resolved { get; }
        public IState Closed { get; }
        public IState CurrentState { get; set; }

        public WorkItem()
        {
            Active = new ActiveState(this);
            Resolved = new ResolvedState(this);
            Closed = new ClosedState(this);
            Proposed = new ProposedState(this);
            CurrentState = Proposed;
        }

        public void MakeActive()
        {
            CurrentState.MakeActive();
        }

        public void Resolve()
        {
            CurrentState.MakeResolved();
        }

        public void Close()
        {
            CurrentState.MakeClosed();
        }

        public void Delete()
        {
            if (CurrentState != Closed)
            {
                throw new InvalidOperationException("Work item must be closed before it is deleted.");
            }
        }
    }
}