﻿namespace CoffeeLib
{
    public abstract class CoffeeDecorator : Coffee
    {
        private readonly Coffee _decoratedCoffee;

        protected CoffeeDecorator(Coffee decoratedCoffee)
        {
            _decoratedCoffee = decoratedCoffee;
        }

        public override string Description => _decoratedCoffee.Description;
        public override decimal Cost => _decoratedCoffee.Cost;
    }
}