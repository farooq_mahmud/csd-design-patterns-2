﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public enum PaymentType
    {
        CreditCard, Paypal
    }
}
