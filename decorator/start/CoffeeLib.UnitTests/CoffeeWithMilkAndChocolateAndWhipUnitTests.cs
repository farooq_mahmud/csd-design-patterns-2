﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeLib.UnitTests
{
    [TestClass]
    public class CoffeeWithMilkAndChocolateAndWhipUnitTests
    {
        private readonly CoffeeWithMilkAndChocolateAndWhip _coffeeWithMilkAndChocolateAndWhip =
            new CoffeeWithMilkAndChocolateAndWhip();

        [TestMethod]
        public void Can_Get_The_Description()
        {
            Assert.AreEqual("coffee black with milk with chocolate with whip",
                _coffeeWithMilkAndChocolateAndWhip.Description);
        }

        [TestMethod]
        public void Cost_Is_190()
        {
            Assert.AreEqual(1.9M, _coffeeWithMilkAndChocolateAndWhip.Cost);
        }
    }
}