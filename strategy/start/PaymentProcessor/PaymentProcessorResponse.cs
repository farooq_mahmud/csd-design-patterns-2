﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public class PaymentProcessorResponse
    {
        public string ConfirmationCode { get; set; }
    }
}