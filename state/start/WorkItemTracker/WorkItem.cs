﻿using System;

namespace WorkItemTracker
{
    public class WorkItem
    {
        public WorkItem()
        {
            CurrentState = WorkItemState.Proposed;
        }

        public WorkItemState CurrentState { get; set; }

        public void MakeActive()
        {
            switch (CurrentState)
            {
                case WorkItemState.Proposed:
                    CurrentState = WorkItemState.Active;
                    break;

                case WorkItemState.Active:
                    throw new InvalidOperationException("Work item is already active.");

                case WorkItemState.Resolved:
                    CurrentState = WorkItemState.Active;
                    break;

                case WorkItemState.Closed:
                    throw new InvalidOperationException("Work item is closed.");
            }
        }

        public void Resolve()
        {
            switch (CurrentState)
            {
                case WorkItemState.Proposed:
                    throw new InvalidOperationException("Proposed work items cannot be resolved.");

                case WorkItemState.Active:
                    CurrentState = WorkItemState.Resolved;
                    break;

                case WorkItemState.Resolved:
                    throw new InvalidOperationException("Work item is already resolved.");

                case WorkItemState.Closed:
                    throw new InvalidOperationException("Work item is closed.");
            }
        }

        public void Close()
        {
            switch (CurrentState)
            {
                case WorkItemState.Proposed:
                    CurrentState = WorkItemState.Closed;
                    break;

                case WorkItemState.Active:
                    throw new InvalidOperationException("Work items must be resolved before closing.");

                case WorkItemState.Resolved:
                    CurrentState = WorkItemState.Closed;
                    break;

                case WorkItemState.Closed:
                    throw new InvalidOperationException("Work item is already closed.");
            }
        }

        public void Delete()
        {
            if (CurrentState != WorkItemState.Closed)
                throw new InvalidOperationException("Only closed work items can be deleted.");
        }
    }
}