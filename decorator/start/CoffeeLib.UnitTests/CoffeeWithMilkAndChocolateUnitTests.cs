﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeLib.UnitTests
{
    [TestClass]
    public class CoffeeWithMilkAndChocolateUnitTests
    {
        private readonly CoffeeWithMilkAndChocolate _coffeeWithMilkAndChocolate = new CoffeeWithMilkAndChocolate();

        [TestMethod]
        public void Can_Get_The_Description()
        {
            Assert.AreEqual("coffee black with milk with chocolate", _coffeeWithMilkAndChocolate.Description);
        }

        [TestMethod]
        public void Cost_Is_180()
        {
            Assert.AreEqual(1.8M, _coffeeWithMilkAndChocolate.Cost);
        }
    }
}