﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorkItemTracker.UnitTests
{
    [TestClass]
    public class ProposedStateUnitTests
    {
        private WorkItem _workItem;

        [TestInitialize]
        public void TestInitialize()
        {
            _workItem = new WorkItem();
            _workItem.CurrentState = new ProposedState(_workItem);
        }

        [TestMethod]
        public void MakeProposed_LeavesWorkItemInProposedState()
        {
            _workItem.CurrentState.MakeProposed();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ProposedState));
        }

        [TestMethod]
        public void MakeActive_SetsWorkItemToActiveState()
        {
            _workItem.CurrentState.MakeActive();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ActiveState));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MakeResolved_ThrowsException()
        {
            _workItem.CurrentState.MakeResolved();
        }

        [TestMethod]
        public void MakeClosed_SetsWorkItemToClosed()
        {
            _workItem.CurrentState.MakeClosed();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ClosedState));
        }
    }
}
