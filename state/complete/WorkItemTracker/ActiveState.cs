﻿using System;

namespace WorkItemTracker
{
    public class ActiveState : State
    {
        public ActiveState(WorkItem workItem) : base(workItem)
        {
        }

        public override void MakeProposed()
        {
            throw new InvalidOperationException("Work items in active state cannot be placed in the proposed state.");
        }

        public override void MakeActive()
        {
            WorkItem.CurrentState = WorkItem.Active;
        }

        public override void MakeResolved()
        {
            WorkItem.CurrentState = WorkItem.Resolved;
        }

        public override void MakeClosed()
        {
            throw new InvalidOperationException("Work items in active state cannot be closed.");
        }
    }
}