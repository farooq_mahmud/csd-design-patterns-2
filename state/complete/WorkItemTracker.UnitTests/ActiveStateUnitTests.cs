﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorkItemTracker.UnitTests
{
    [TestClass]
    public class ActiveStateUnitTests
    {
        private WorkItem _workItem;

        [TestInitialize]
        public void TestInitialize()
        {
            _workItem = new WorkItem();
            _workItem.CurrentState = new ActiveState(_workItem);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MakeProposed_ThrowsException()
        {
            _workItem.CurrentState.MakeProposed();
        }

        [TestMethod]
        public void MakeActive_LeavesWorkItemInActiveState()
        {
            _workItem.CurrentState.MakeActive();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ActiveState));
        }

        [TestMethod]
        public void MakeResolved_SetsWorkItemToResolvedState()
        {
            _workItem.CurrentState.MakeResolved();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ResolvedState));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MakeClosed_ThrowsException()
        {
            _workItem.CurrentState.MakeClosed();
        }
    }
}
