﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorkItemTracker.UnitTests
{
    [TestClass]
    public class ClosedStateUnitTests
    {
        private WorkItem _workItem;

        [TestInitialize]
        public void TestInitialize()
        {
            _workItem = new WorkItem();
            _workItem.CurrentState = new ClosedState(_workItem);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MakeProposed_ThrowsException()
        {
            _workItem.CurrentState.MakeProposed();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MakeActive_ThrowsException()
        {
            _workItem.CurrentState.MakeActive();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MakeResolved_ThrowsException()
        {
            _workItem.CurrentState.MakeResolved();
        }

        [TestMethod]
        public void MakeClosed_SetsWorkItemStateToClose()
        {
            _workItem.CurrentState.MakeClosed();
            Assert.IsInstanceOfType(_workItem.CurrentState, typeof(ClosedState));
        }
    }
}
