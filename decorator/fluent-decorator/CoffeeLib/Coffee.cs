﻿namespace CoffeeLib
{
    public class Coffee : Drink
    {
        public override string Description => "coffee black";
        public override decimal Cost => 1.50M;
    }
}