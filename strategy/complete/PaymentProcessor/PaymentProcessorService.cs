﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public class PaymentProcessorService : IPaymentProcessorService
    {
        private readonly IPaymentProcessingStrategy _paymentProcessingStrategy;

        public PaymentProcessorService(IPaymentProcessingStrategy paymentProcessingStrategy)
        {
            _paymentProcessingStrategy = paymentProcessingStrategy;
        }

        public PaymentProcessorResponse ProcessPayment(Order order)
        {
            return _paymentProcessingStrategy.ProcessPayment(order);
        }
    }
}