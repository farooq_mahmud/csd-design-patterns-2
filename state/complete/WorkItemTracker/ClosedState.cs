﻿using System;

namespace WorkItemTracker
{
    public class ClosedState : State
    {
        public ClosedState(WorkItem workItem) : base(workItem)
        {
        }

        public override void MakeProposed()
        {
            throw new InvalidOperationException("Work items in closed state cannot be proposed.");
        }

        public override void MakeActive()
        {
            throw new InvalidOperationException("Work items in closed state cannot be active.");
        }

        public override void MakeResolved()
        {
            throw new InvalidOperationException("Work items in closed state cannot be resolved.");
        }

        public override void MakeClosed()
        {
            WorkItem.CurrentState = WorkItem.Closed;
        }
    }
}