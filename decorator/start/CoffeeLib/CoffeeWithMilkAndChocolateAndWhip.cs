﻿namespace CoffeeLib
{
    public class CoffeeWithMilkAndChocolateAndWhip : CoffeeWithMilkAndChocolate
    {
        public override string Description => $"{base.Description} with whip";
        public override decimal Cost => base.Cost + 0.10M;
    }
}