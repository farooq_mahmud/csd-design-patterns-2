﻿using System;

namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public static class PaymentProcessorStrategies
    {
        public static Func<Order, PaymentProcessorResponse> CreditCardPaymentProcessorStrategy =
            order => new PaymentProcessorResponse {ConfirmationCode = "cc_1234"};

        public static Func<Order, PaymentProcessorResponse> PaypalPaymentProcessorStrategy =
            order => new PaymentProcessorResponse {ConfirmationCode = "pp_1234"};
    }
}