﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public class PaypalPaymentProcessingStrategy : IPaymentProcessingStrategy
    {
        public PaymentProcessorResponse ProcessPayment(Order order)
        {
            return new PaymentProcessorResponse {ConfirmationCode = "pp_123456"};
        }
    }
}