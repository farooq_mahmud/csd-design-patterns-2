﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CsdDesignPatterns.Strategy.PaymentProcessor.UnitTests
{
    [TestClass]
    public class PaymentProcessorServiceUnitTests
    {
        private readonly IPaymentProcessorService _paymentProcessorService = new PaymentProcessorService();
        
        [TestMethod]
        public void ProcessPayment_WhenPayingByCreditCard_ConfirmationCodeBeginsWith_cc()
        {
            var response = _paymentProcessorService.ProcessPayment(TestOrderFactory.CreateCreditCardOrder());
            Assert.IsTrue(response.ConfirmationCode.StartsWith("cc"));
        }

        [TestMethod]
        public void ProcessPayment_WhenPayingByPaypal_ConfirmationCodeBeginsWith_pp()
        {
            var response = _paymentProcessorService.ProcessPayment(TestOrderFactory.CreatePaypalOrder());
            Assert.IsTrue(response.ConfirmationCode.StartsWith("pp"));
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ProcessPayment_WhenPaymentMethodIsInvalid_ExceptionIsThrown()
        {
            var response = _paymentProcessorService.ProcessPayment(TestOrderFactory.CreateOrderWithInvalidPaymentType());
        }
    }
}
