﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CoffeeLib.FluentApi.UnitTests
{
    [TestClass]
    public class CoffeeExtensionsUnitTests
    {
        [TestMethod]
        public void Make_And_Verify_Coffee_Using_FluentApi()
        {
            var coffee = new Coffee().WithMilk().WithChocolate().WithWhip();
            Assert.AreEqual("coffee black with milk with chocolate with whip", coffee.Description);
            Assert.AreEqual(1.9M, coffee.Cost);
        }
    }
}