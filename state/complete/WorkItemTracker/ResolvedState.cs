﻿using System;

namespace WorkItemTracker
{
    public class ResolvedState : State
    {
        public ResolvedState(WorkItem workItem) : base(workItem)
        {
        }

        public override void MakeProposed()
        {
            throw new InvalidOperationException("Work items in the resolved state cannot be proposed.");
        }

        public override void MakeActive()
        {
            WorkItem.CurrentState = WorkItem.Active;
        }

        public override void MakeResolved()
        {
            WorkItem.CurrentState = WorkItem.Resolved;
        }

        public override void MakeClosed()
        {
            WorkItem.CurrentState = WorkItem.Closed;
        }
    }
}