﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor.UnitTests
{
    internal static class TestOrderFactory
    {
        internal static Order CreateCreditCardOrder()
        {
            return new Order {PaymentType = PaymentType.CreditCard, Amount = 100M, Description = "Credit Card Order"};
        }

        internal static Order CreatePaypalOrder()
        {
            return new Order {PaymentType = PaymentType.Paypal, Amount = 100M, Description = "Paypal Order"};
        }

        internal static Order CreateOrderWithInvalidPaymentType()
        {
            return new Order {PaymentType = (PaymentType) 300, Amount = 100M, Description = "Paypal Order"};
        }
    }
}