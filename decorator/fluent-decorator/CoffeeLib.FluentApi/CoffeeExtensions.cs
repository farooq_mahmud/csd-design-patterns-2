﻿namespace CoffeeLib.FluentApi
{
    public static class CoffeeExtensions
    {
        public static Coffee WithMilk(this Coffee coffee)
        {
            return new WithMilk(coffee);
        }

        public static Coffee WithWhip(this Coffee coffee)
        {
            return new WithWhip(coffee);
        }

        public static Coffee WithChocolate(this Coffee coffee)
        {
            return new WithChocolate(coffee);
        }
    }
}