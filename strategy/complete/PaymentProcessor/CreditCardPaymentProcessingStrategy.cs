﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public class CreditCardPaymentProcessingStrategy : IPaymentProcessingStrategy
    {
        public PaymentProcessorResponse ProcessPayment(Order order)
        {
            return new PaymentProcessorResponse {ConfirmationCode = "cc_123456"};
        }
    }
}