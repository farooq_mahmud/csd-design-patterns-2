﻿namespace WorkItemTracker
{
    public enum WorkItemState
    {
        Proposed,
        Active,
        Resolved,
        Closed
    }
}