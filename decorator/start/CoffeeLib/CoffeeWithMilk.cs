﻿namespace CoffeeLib
{
    public class CoffeeWithMilk : Coffee
    {
        public override string Description => $"{base.Description} with milk";
        public override decimal Cost => base.Cost + 0.25M;
    }
}