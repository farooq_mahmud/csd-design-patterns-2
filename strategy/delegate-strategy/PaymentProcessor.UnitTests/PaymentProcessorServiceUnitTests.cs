﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CsdDesignPatterns.Strategy.PaymentProcessor.UnitTests
{
    [TestClass]
    public class PaymentProcessorServiceUnitTests
    {
        private IPaymentProcessorService _paymentProcessorService;

        [TestMethod]
        public void ProcessPayment_WhenPayingByCreditCard_ConfirmationCodeBeginsWith_cc()
        {

            _paymentProcessorService = new PaymentProcessorService(PaymentProcessorStrategies.CreditCardPaymentProcessorStrategy);
            var response = _paymentProcessorService.ProcessPayment(TestOrderFactory.CreateCreditCardOrder());
            Assert.IsTrue(response.ConfirmationCode.StartsWith("cc"));
        }

        [TestMethod]
        public void ProcessPayment_WhenPayingByPaypal_ConfirmationCodeBeginsWith_pp()
        {
            _paymentProcessorService = new PaymentProcessorService(PaymentProcessorStrategies.PaypalPaymentProcessorStrategy);
            var response = _paymentProcessorService.ProcessPayment(TestOrderFactory.CreatePaypalOrder());
            Assert.IsTrue(response.ConfirmationCode.StartsWith("pp"));
        }
    }
}