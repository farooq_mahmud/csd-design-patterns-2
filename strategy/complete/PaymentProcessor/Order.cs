﻿namespace CsdDesignPatterns.Strategy.PaymentProcessor
{
    public class Order
    {
        public PaymentType PaymentType { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }
}