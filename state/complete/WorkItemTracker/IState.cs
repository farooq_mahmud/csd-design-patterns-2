﻿namespace WorkItemTracker
{
    public interface IState
    {
        void MakeProposed();
        void MakeActive();
        void MakeResolved();
        void MakeClosed();
    }
}